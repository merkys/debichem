debichem (0.0.12) UNRELEASED; urgency=medium

  [ Michael Banck ]
  * tasks/semiempirical: Added mopac and xtb.
  * tasks/molecular-abinitio: Added openmolcas.
  * tasks/development: Added libdbcsr-dev and libbtas-dev.

  [ Andreas Tille ]
  * tasks/analytical-biochemistry: Added comet-ms

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sun, 07 Feb 2021 23:43:04 +0100

debichem (0.0.11) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * DEP5 copyright

  [ Filippo Rusconi ]
  * tasks/analytical-biochemistry: Added minexpert2, xtpcpp, toppic as Depends,
    and libpwizlite-dev and libisospec++-dev as Suggests.
  * tasks/analytical-biochemistry: Update description.

  [ Michael Banck ]
  * tasks/cheminformatics: Added libopenchemlib-java.
  * tasks/development: Added libopenmm-dev and libsymspg-dev.
  * tasks/input-generation-output-processing: Addedd c2x and python3-pycifrw.
  * tasks/molecular-modelling: Added mmb and openstructure.
  * tasks/periodic-abinitio: Added wannier90.

 -- Michael Banck <mbanck@debian.org>  Sun, 07 Feb 2021 23:42:29 +0100

debichem (0.0.10) unstable; urgency=medium

  * Team upload.
  * Remove final Python 2 module packages (Closes: #945634).

 -- Stuart Prescott <stuart@debian.org>  Sun, 12 Jan 2020 23:59:53 +1100

debichem (0.0.9) unstable; urgency=medium

  * Team Upload.

  [ Graham Inggs ]
  * tasks/input-generation-output-processing: python-ase is now ase
  * tasks/input-generation-output-processing: remove p4vasp, see #937227
  * tasks/visualisation: ditto

  [ Stuart Prescott ]
  * Remove dependency on libblacs-mpi-dev (Closes: #878019).
  * Stop mentioning removed packages (Closes: #891804).
  * Update Standards-Version to 4.4.1 (no changes required).
  * Add Rules-Requires-Root: no.
  * Switch to debhelper-compat (= 12).

 -- Stuart Prescott <stuart@debian.org>  Fri, 10 Jan 2020 00:48:52 +1100

debichem (0.0.8) unstable; urgency=medium

  * Depends: ${misc:Depends} for debichem-tasks
  * lintian-override for false positive
  * Re-render tasks with Buster release state

 -- Andreas Tille <tille@debian.org>  Thu, 13 Jun 2019 09:16:07 +0200

debichem (0.0.7) unstable; urgency=medium

  * Moved from Alioth to Salsa
  * Standards-Version: 4.3.0
  * Priority: optional

 -- Andreas Tille <tille@debian.org>  Mon, 14 Jan 2019 19:49:32 +0100

debichem (0.0.6) unstable; urgency=medium

  [ Michael Banck ]
  * tasks/modelling: Added ballview.
  * tasks/visualization: Removed travis.
  * tasks/view-edit-2d: Update task title and description.
  * tasks/abinitio: Split up to ...
  * tasks/molecular-abinitio: ... this and ...
  * tasks/periodic-abinitio: ... this.
  * tasks/molmech: Renamed to ...
  * tasks/molecular-dynamics: ... this.
  * tasks/molecular-dynamics: Removed ghemical, avogadro and vmd, added
    dl-poly-classic, cp2k and nwchem.
  * tasks/polymer: Renamed to ...
  * tasks/analytical-biochemistry: ... this.
  * tasks/input-generation-output-processing: New task.
  * tasks/development: New task.
  * tasks/molecular-abinitio, tasks/molecular-dynamics,
    tasks/periodic-abinitio: Update task title and description.
  * tasks/molecular-abinitio: Added bagel.
  * tasks/molecular-abinitio: Replaced libchemps2 with chemps2.
  * tasks/periodic-abinitio: Added gpaw.
  * tasks/molecular-abinitio: Added mpqc3.
  * tasks/development: Added libga-dev.
  * tasks/modelling: Renamed to ...
  * tasks/molecular-modelling: ... this.

  [ Andreas Tille ]
  * rebuild using blends-dev 0.6.95
  * source/format: 3.0 (native)
  * Bump Standards-Version and debhelper compat level

 -- Michael Banck <mbanck@debian.org>  Tue, 24 Jan 2017 22:33:29 +0100

debichem (0.0.5) unstable; urgency=medium

  * debian/source/format: Removed.

 -- Michael Banck <mbanck@debian.org>  Mon, 08 Dec 2014 20:53:53 +0100

debichem (0.0.4) unstable; urgency=medium

  [ Michael Banck ]
  * tasks/abinitio: Added psi4.
  * tasks/crystallography: New task, including shelxle and xcrysden.
  * tasks/visualisation: Moved gcrystal to ...
  * tasks/crystallography: ... this.
  * tasks/crystallography: Added python-fabio.
  * tasks/molmech: Added lammps.
  * tasks/semiempirical: Added molds.
  * tasks/abinitio: Added elk-lapw.
  * tasks/abinitio: Added ergo.
  * tasks/visualisation, tasks/crystallography: Added gamgi.
  * tasks/visualisation: Added p4vasp.
  * tasks/visualisation: Added travis.
  * tasks/molmech: Added votca-csg.
  * tasks/polymer: Changed r-cran-mixtools and r-other-amsmercury Suggests to
    Depends.
  * tasks/polymer: Added r-other-iwrlars.

  [ Daniel Leidert ]
  * tasks/crystallography: Added drawxtl.

  [ Andreas Tille ]
  * d/source/format: 3.0 (native)

 -- Michael Banck <mbanck@debian.org>  Fri, 05 Dec 2014 16:45:37 +0100

debichem (0.0.3) unstable; urgency=low

  [ Michael Banck ]
  * tasks/visualisation: Added jmol and qutemol.
  * tasks/visualisation: Added cclib.
  * tasks/semiempirical: Added cp2k.
  * tasks/cheminformatics: Added python-indigo.
  * tasks/visualization: Added shelxle, xcrysden and drawxtl.
  * tasks/cheminformatics: Added python-fmcs, python-chemfp and libopsin-java.
  * tasks/visualization: Added gamgi.
  * tasks/molmech, tasks/visualization, tasks/cheminformatics: Removed some
    citation data which is already present in the respective debian/upstream
    files.
  * tasks/visualization: Added ballview, raster3d and kalzium.

  [ Daniel Leidert ]
  * debian/control.stub (Standards-Version): Bumped to 3.9.3.
    (Vcs-Browser): Adjusted.

 -- Michael Banck <mbanck@debian.org>  Sun, 24 Feb 2013 16:30:10 +0100

debichem (0.0.2) unstable; urgency=low

  [ Andreas Tille ]
  * tasks/modelling: Added python-mmtk and nmoldyn.
  * tasks/visualisation: Added publication info to rasmol.
  * tasks/view-edit-2d: Added osra as prospective package.

  [ Michael Banck ]
  * tasks/abinitio: Added nwchem, aces3, cp2k and quantum-espresso.
  * Added and/or updated publication/registration info for adun.app, gromacs,
    massxpert, mmass, gabedit, rasmol, psi3, abinit, nwchem, aces3 and
    quantum-espresso.
  * tasks/cheminformatics: New task.
  * debian/control: Updated.

 -- Michael Banck <mbanck@debian.org>  Tue, 01 Nov 2011 23:53:00 +0100

debichem (0.0.1) unstable; urgency=low

  * Initial release. (Closes: #589042)

 -- Andreas Tille <tille@debian.org>  Mon, 19 Jul 2010 22:44:13 +0200
